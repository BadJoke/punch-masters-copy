﻿using DG.Tweening;
using UnityEngine;

namespace Player
{
    public class CameraWobble : MonoBehaviour
    {
        [SerializeField] private float _angle = 2f;

        private Vector3 _startEulers;
        private Vector3 _first;
        private Vector3 _second;
        private Sequence _wobble;

        private void Start()
        {
            _startEulers = transform.localRotation.eulerAngles;
            _first = _startEulers + Vector3.forward * _angle;
            _second = _startEulers + Vector3.back * _angle;

            //StartWobble();
        }

        public void Play()
        {
            StartWobble();
        }

        public void Pause()
        {
            _wobble.Kill();
            transform.DOLocalRotate(_startEulers, 0.5f);
        }

        private void StartWobble()
        {
            _wobble = DOTween.Sequence()
                .Append(transform.DOLocalRotate(_first, 0.5f))
                .Append(transform.DOLocalRotate(_second, 1f))
                .Append(transform.DOLocalRotate(_startEulers, 0.5f))
                .SetLoops(-1);
        }
    }
}
