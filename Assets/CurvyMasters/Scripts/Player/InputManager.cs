﻿using UnityEngine;
using UnityEngine.Events;

namespace Player
{
    public class InputManager : MonoBehaviour
    {
        public event UnityAction<Vector3> Clicked;

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Clicked?.Invoke(Input.mousePosition);
            }
        }
    }
}