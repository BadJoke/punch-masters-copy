﻿using Enemy;
using Enemy.Partman;
using Enemy.Humanoid;
using FUGames.Pooling;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Player.Punching
{
    [RequireComponent(typeof(BoxCollider))]
    public class Fist : MonoBehaviour
    {
        [SerializeField] private float _punchForce = 15f;
        [SerializeField] private int _damage;
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private GameObject _arm;

        private Collider _collider;
        private PoolManager _poolManager;

        public event UnityAction Collided;
        public event UnityAction Punched;

        [Inject]
        private void Constructor(PoolManager poolManager)
        {
            _poolManager = poolManager;
        }

        private void Awake()
        {
            _collider = GetComponent<Collider>();
        }

        private void OnCollisionEnter(Collision collision)
        {
            CheckCollision(collision.gameObject);
        }

        private void OnTriggerEnter(Collider other)
        {
            CheckCollision(other.gameObject);
        }

        public void Enable()
        {
            _arm.SetActive(false);
            gameObject.SetActive(true);

            _collider.enabled = true;
        }

        public void Disable()
        {
            gameObject.SetActive(false);
            _arm.SetActive(true);
        }

        public void MoveTo(Vector3 position)
        {
            transform.localPosition = position;
        }

        private void CheckCollision(GameObject other)
        {
            _collider.enabled = false;
            Collided?.Invoke();

            if (other.TryGetComponent(out IDamagable damagable) == false)
            {
                return;
            }

            _poolManager.Take<HitEffect>().Play(transform.position);

            var damageData = new DamageData(_damage, _punchForce, transform.position);
            damagable.TakeDamage(damageData);

            Punched?.Invoke();
        }
    }
}