﻿using UnityEngine;
using UnityEngine.Events;

namespace Player.Punching
{
    [RequireComponent(typeof(InputManager))]
    public class PunchController : MonoBehaviour
    {
        [SerializeField] private float _maxRayDistance = 50f;
        [SerializeField] private float _movingTime = 50f;
        [SerializeField] private FistMover _leftFist;
        [SerializeField] private FistMover _rightFist;
        [SerializeField] private LayerMask _enemyLayer;

        private Camera _camera;
        private FistMover _currentFist;
        private InputManager _input;

        private const int EnemyLayer = 9;
        private const int FloorLayer = 10;
        private const int BorderLayer = 12;

        public event UnityAction Punching;
        public event UnityAction Punched;

        private void OnEnable()
        {
            _input.Clicked += OnClicked;
            _leftFist.Moved += OnFistMoved;
            _rightFist.Moved += OnFistMoved;
        }

        private void Awake()
        {
            _camera = Camera.main;
            _input = GetComponent<InputManager>();
            _currentFist = _rightFist;

            _leftFist.SetMovingTime(_movingTime);
            _rightFist.SetMovingTime(_movingTime);
        }

        private void OnDisable()
        {
            _input.Clicked -= OnClicked;
            _leftFist.Moved -= OnFistMoved;
            _rightFist.Moved -= OnFistMoved;
        }

        private void TryPunch(RaycastHit hit, bool isStatic)
        {
            if (_currentFist.IsWaiting == false)
            {
                return;
            }

            if (isStatic)
            {
                _currentFist.StaticPunch(hit.point);
            }
            else
            {
                _currentFist.TargetedPunch(hit.collider);
            }

            _currentFist = _currentFist.Couple;

            Punching?.Invoke();
        }

        private void OnClicked(Vector3 position)
        {
            var ray = _camera.ScreenPointToRay(position);

            if (Physics.Raycast(ray, out RaycastHit hit, _maxRayDistance, _enemyLayer.value))
            {
                switch (hit.collider.gameObject.layer)
                {
                    case EnemyLayer:
                        TryPunch(hit, false);
                        break;
                    case FloorLayer:
                    case BorderLayer:
                        TryPunch(hit, true);
                        break;
                }
            }
        }

        private void OnFistMoved(FistMover mover)
        {
            if (mover.Couple.IsWaiting)
            {
                Punched?.Invoke();
            }
        }
    }
}