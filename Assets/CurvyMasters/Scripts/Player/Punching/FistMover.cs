﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Player.Punching
{
    [RequireComponent(typeof(CurveMeshGenerator))]
    public class FistMover : MonoBehaviour
    {
        [SerializeField] private float _movingTime;
        [SerializeField] private Vector3 _power;
        [SerializeField] private Fist _fist;
        [SerializeField] private Target _target;
        [SerializeField] private FistMover _couple;

        private bool _isMoveBack = false;
        private Vector3 _end;
        private Coroutine _moving;
        private Curve _curve;
        private CurveMeshGenerator _generator;

        public event UnityAction<FistMover> Moved;

        public bool IsWaiting => _moving == null;
        public FistMover Couple => _couple;

        private void OnEnable()
        {
            _fist.Collided += OnFistCollided;
        }

        private void Awake()
        {
            _curve = new Curve(Vector3.zero, _power, _movingTime);
            _generator = GetComponent<CurveMeshGenerator>();
        }

        private void OnDisable()
        {
            _fist.Collided -= OnFistCollided;
        }

        public void SetMovingTime(float time)
        {
            _movingTime = time;
        }

        public void StaticPunch(Vector3 position)
        {
            _isMoveBack = false;
            _target.StayAt(position);
            _moving = StartCoroutine(Moving());
        }

        public void TargetedPunch(Collider target)
        {
            _isMoveBack = false;
            _target.FollowAt(target);
            _moving = StartCoroutine(Moving());
        }

        private IEnumerator Moving()
        {
            var time = 0f;

            _fist.Enable();

            do
            {
                UpdateTime(ref time);
                Move(time);

                if (time >= _movingTime)
                {
                    _isMoveBack = true;
                }

                yield return new WaitForFixedUpdate();
            }
            while (time > 0);

            FinishMoving();
        }

        private void Move(float time)
        {
            _end = _target.transform.localPosition;

            var points = _curve.Spawn(_end, time, _isMoveBack);
            var fistPosition = GetFistPosition(points);

            _generator.Generate(points);
            _fist.MoveTo(fistPosition);
            _curve.RotateFist(_fist.transform);
        }

        private void FinishMoving()
        {
            _moving = null;

            _fist.Disable();

            Moved?.Invoke(this);
        }

        private Vector3 GetFistPosition(List<CurvePoint> points)
        {
            if (points.Count > 0)
            {
                return points[points.Count - 1].Position;
            }
            else
            {
                return Vector3.zero;
            }
        }

        private void UpdateTime(ref float time)
        {
            if (_isMoveBack)
            {
                time -= Time.fixedDeltaTime * 0.5f;
            }
            else
            {
                var ratio = 1f - (time / _movingTime * 0.98f);
                time += Time.fixedDeltaTime * ratio;
            }
        }

        private void OnFistCollided()
        {
            _isMoveBack = true;
        }
    }
}