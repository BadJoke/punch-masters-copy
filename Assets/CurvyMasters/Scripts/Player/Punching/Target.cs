﻿using UnityEngine;

namespace Player.Punching
{
    public class Target : MonoBehaviour
    {
        private Collider _target;

        private void Awake()
        {
            enabled = false;
        }

        private void Update()
        {
            transform.position = _target.bounds.center;
        }

        public void FollowAt(Collider target)
        {
            _target = target;
            transform.position = _target.bounds.center;
            enabled = true;
        }

        public void StayAt(Vector3 position)
        {
            enabled = false;
            transform.position = position;
        }
    }
}