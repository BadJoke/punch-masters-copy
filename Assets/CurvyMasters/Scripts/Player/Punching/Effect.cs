﻿using FUGames.Pooling;
using UnityEngine;

namespace Player.Punching
{
    public abstract class Effect : PoolObject
    {
        [SerializeField] private ParticleSystem _effect;

        private void OnParticleSystemStopped()
        {
            BackToPool();
        }

        public void Play(Vector3 position)
        {
            transform.position = position;

            gameObject.SetActive(true);
            _effect.Play();
        }
    }
}
