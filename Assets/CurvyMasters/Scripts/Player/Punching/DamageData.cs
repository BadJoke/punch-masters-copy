﻿using UnityEngine;

namespace Player.Punching
{
    public class DamageData
    {
        public int Damage { get; private set; }
        public float Force { get; private set; }
        public Vector3 Position { get; private set; }

        public DamageData(int damage, float force, Vector3 position)
        {
            Damage = damage;
            Force = force;
            Position = position;
        }
    }
}