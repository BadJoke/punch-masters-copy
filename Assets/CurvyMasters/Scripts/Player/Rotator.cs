﻿using UnityEngine;
using Waypoints;

namespace Player
{
    public class Rotator
    {
        private Transform _transform;
        private Mover _mover;

        public Rotator(Transform transform, Mover mover)
        {
            _transform = transform;
            _mover = mover;
        }

        public void Rotate(Waypoint waypoint, float distancePercentage)
        {
            if (waypoint.IsFightable && distancePercentage < waypoint.StartCombatThreshold)
            {
                float currentSpeed = 10f;

                _transform.rotation = Quaternion.Slerp(
                    _transform.rotation,
                    GetFinalRotation(waypoint.CombatLookPoint.position),
                    currentSpeed * Time.unscaledDeltaTime);
            }
            else if (distancePercentage <= waypoint.StartRotationThreshold
                && distancePercentage >= waypoint.StopRotationThreshold
                && waypoint.AddLookOption > 0)
            {
                float currentSpeed = 2f;

                switch (waypoint.AddLookOption)
                {
                    case ExtraLookOption.NextOfNextPoint:
                        {
                            if (waypoint.NextPoint.NextPoint != null)
                                _transform.rotation = Quaternion.Slerp(
                                    _transform.rotation,
                                    GetFinalRotation(waypoint.NextPoint.NextPoint.Position),
                                    currentSpeed * Time.deltaTime);
                            break;
                        }
                    case ExtraLookOption.SpecialPoint:
                        {
                            if (waypoint.SpecialLookPoint != null)
                                _transform.rotation = Quaternion.Slerp(
                                    _transform.rotation,
                                    GetFinalRotation(waypoint.SpecialLookPoint.position),
                                    currentSpeed * Time.deltaTime);
                            break;
                        }
                }
            }
            else
            {
                float currentSpeed = 5f;

                switch (waypoint.DefaultLookOption)
                {
                    case BaseLookOption.Forward:
                        {
                            _transform.rotation = Quaternion.Slerp(
                                _transform.rotation,
                                Quaternion.LookRotation(_mover.MovementDirection),
                                currentSpeed * Time.deltaTime);
                            break;
                        }
                    case BaseLookOption.NextPoint:
                        {
                            _transform.rotation = Quaternion.Slerp(
                                _transform.rotation,
                                GetFinalRotation(waypoint.NextPoint.Position),
                                currentSpeed * Time.deltaTime);
                            break;
                        }
                }
            }
        }

        private Quaternion GetFinalRotation(Vector3 lookTarget)
        {
            Vector3 direction = lookTarget - _transform.position;

            return direction != Vector3.zero ? Quaternion.LookRotation(direction) : _transform.rotation;
        }
    }
}
