﻿using Player.Punching;
using UnityEngine;

namespace Player
{
    public class PlayerAnimator : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private PlayerMovement _movement;
        [SerializeField] private PunchController _punchController;
        [SerializeField] private Fist[] _fists;
        [SerializeField] private CameraShaker _shaker;

        private void OnEnable()
        {
            _movement.Moving += OnPlayerMoving;
            _movement.Moved += OnPlayerMoved;
            _punchController.Punching += OnPunching;
            _punchController.Punched += OnPunched;

            foreach (var fist in _fists)
            {
                fist.Punched += OnFistPunched;
            }
        }

        private void Awake()
        {
            _animator.speed = 0f;
        }

        private void OnDisable()
        {
            _movement.Moving -= OnPlayerMoving;
            _movement.Moved -= OnPlayerMoved;
            _punchController.Punching -= OnPunching;
            _punchController.Punched -= OnPunched;

            foreach (var fist in _fists)
            {
                fist.Punched -= OnFistPunched;
            }
        }

        private void OnPlayerMoving()
        {
            _animator.speed = 1f;
        }

        private void OnPlayerMoved()
        {
            _animator.speed = 0f;
        }

        private void OnFistPunched()
        {
            _shaker.Shake();
        }

        private void OnPunching()
        {
            _animator.enabled = false;
        }

        private void OnPunched()
        {
            _animator.enabled = true;
            _animator.recorderStartTime = 0f;
        }
    }
}
