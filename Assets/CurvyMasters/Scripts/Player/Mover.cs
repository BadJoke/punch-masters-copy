﻿using UnityEngine;
using Waypoints;

namespace Player
{
    public class Mover
    {
        public Vector3 MovementDirection { get; private set; }

        private Transform _transform;
        private float _currentSpeed;
        private float _defaultSpeed;
        private float _acceleration;

        public Mover(Transform transform, float defaultSpeed, float acceleration)
        {
            _transform = transform;
            _defaultSpeed = defaultSpeed;
            _acceleration = acceleration;
            _currentSpeed = 0f;
        }

        public float CalculateSpeed(Waypoint waypoint, int subpointIndex, float distPercentage)
        {
            var data = new MoveData(
                subpointIndex,
                distPercentage,
                _defaultSpeed,
                _currentSpeed,
                _acceleration);

            _currentSpeed = waypoint.GetSpeed(data);
            return _currentSpeed;
        }

        public void Move(Vector3 targetPosition)
        {
            _transform.position = Vector3.MoveTowards(_transform.position, targetPosition, _currentSpeed * Time.deltaTime);
            MovementDirection = targetPosition - _transform.position;

            if (MovementDirection == Vector3.zero)
                MovementDirection = _transform.forward;
        }
    }
}
