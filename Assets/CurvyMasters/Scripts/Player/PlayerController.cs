﻿using UnityEngine;
using UnityEngine.Events;

namespace Player
{
    [RequireComponent(typeof(InputManager), typeof(PlayerMovement))]
    public class PlayerController : MonoBehaviour
    {
        private InputManager _input;
        private PlayerMovement _movement;

        public event UnityAction Died;

        private void Awake()
        {
            _input = GetComponent<InputManager>();
            _movement = GetComponent<PlayerMovement>();

            _movement.LastPointReached += OnLastPointReached;
        }

        public void StartLevel()
        {
            _input.enabled = true;
            _movement.Begin();
        }

        public void TakeHit()
        {
            _input.enabled = false;
            _movement.Stop();

            Died?.Invoke();
        }

        private void OnLastPointReached()
        {
            _movement.LastPointReached -= OnLastPointReached;

            _input.enabled = false;
        }
    }
}
