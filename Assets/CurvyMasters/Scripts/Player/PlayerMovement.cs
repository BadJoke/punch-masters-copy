﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Waypoints;
using Zenject;

namespace Player
{
    [RequireComponent(typeof(PlayerController))]
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float _defaultSpeed;
        [SerializeField] private float _acceleration;
        [SerializeField] private Waypoint _targetPoint;

        private Coroutine _movement;
        private Mover _mover;
        private Rotator _rotator;

        public event UnityAction Moving;
        public event UnityAction Moved;
        public event UnityAction LastPointReached;

        [Inject]
        private void Constructor(Waypoint startPoint)
        {
            _targetPoint = startPoint;
        }

        private void Awake()
        {
            SetStartParameters();

            enabled = false;

            _mover = new Mover(transform, _defaultSpeed, _acceleration);
            _rotator = new Rotator(transform, _mover);
        }

        private void Update()
        {
            if (_targetPoint.NextPoint == null)
            {
                enabled = false;

                Moved?.Invoke();
                LastPointReached?.Invoke();
            }

            if (_movement == null)
            {
                _movement = StartCoroutine(Movement());
            }
        }

        public void Begin()
        {
            enabled = true;

            _targetPoint.WasReached();
        }

        public void Stop()
        {
            StopCoroutine(_movement);

            enabled = false;
        }

        private void SetStartParameters()
        {
            if (_targetPoint.SpecialLookPoint)
            {
                transform.LookAt(_targetPoint.SpecialLookPoint);
            }    
            else
            {
                var forward = _targetPoint.Path[1] - _targetPoint.Path[0];
                transform.forward = forward.normalized;
            }

            transform.position = _targetPoint.Position;
        }
        
        private IEnumerator Movement()
        {
            float generalDistance = GetDistance(_targetPoint.Path);
            float distanceProgress = 1f;
            
            if (_targetPoint.IsStayable)
            {
                Moved.Invoke();
            }

            for (int i = 0; i < _targetPoint.Path.Count - 1; i++)
            {
                float localDistanceProgress = 1f;

                while (localDistanceProgress > 0f)
                {
                    var speed = _mover.CalculateSpeed(_targetPoint, i, distanceProgress);
                    _mover.Move(_targetPoint.Path[i + 1]);
                    _rotator.Rotate(_targetPoint, distanceProgress);

                    localDistanceProgress = GetDistanceProgress(_targetPoint.Path[i], _targetPoint.Path[i + 1]);
                    distanceProgress = GetDistance(_targetPoint.Path, i) / generalDistance;

                    if (_targetPoint.IsFightable && speed != 0)
                    {
                        Moving?.Invoke();
                    }
                    else
                    {
                        Moved?.Invoke();
                    }

                    yield return null;
                }
            }

            _targetPoint.NextPoint?.WasReached();
            _targetPoint = _targetPoint.NextPoint;
            _movement = null;
        }

        private float GetDistanceProgress(Vector3 startPosition, Vector3 finalPosition)
        {
            float generalDistance = (finalPosition - startPosition).magnitude;
            float currentDistance = (finalPosition - transform.position).magnitude;

            return currentDistance / generalDistance;
        }

        private float GetDistance(List<Vector3> subpoints)
        {
            float distance = 0f;

            for (int i = 0; i < subpoints.Count - 1; i++)
            {
                distance += (subpoints[i + 1] - subpoints[i]).magnitude;
            }

            return distance;
        }

        private float GetDistance(List<Vector3> subpoints, int currentIndex)
        {
            float distance = (subpoints[currentIndex + 1] - transform.position).magnitude;

            for (int i = currentIndex + 1; i < subpoints.Count - 1; i++)
            {
                distance += (subpoints[i + 1] - subpoints[i]).magnitude;
            }

            return distance;
        }
    }
}
