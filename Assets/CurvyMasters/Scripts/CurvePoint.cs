﻿using UnityEngine;

public struct CurvePoint
{
    public float Time { get; private set; }
    public Vector3 Position {get; private set;}

    public CurvePoint(float time, Vector3 position)
    {
        Time = time;
        Position = position;
    }
}