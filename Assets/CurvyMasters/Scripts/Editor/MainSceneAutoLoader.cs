﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

[InitializeOnLoad]
public static class MainSceneAutoLoader
{
    private const string MainScenePath = "Assets/CurvyMasters/Scenes/Loader.unity";
    private const string PreviousScenePathKey = "Previous scene";

    static MainSceneAutoLoader()
    {
        EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
    }

    private static void OnPlayModeStateChanged(PlayModeStateChange state)
    {
        if (!EditorApplication.isPlaying && EditorApplication.isPlayingOrWillChangePlaymode)
        {
            var activeScene = SceneManager.GetActiveScene();

            if (activeScene.buildIndex == 0)
            {
                return;
            }

            PlayerPrefs.SetString(PreviousScenePathKey, activeScene.path);
            PlayerPrefs.Save();

            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            {
                try
                {
                    EditorSceneManager.OpenScene(MainScenePath);
                }
                catch (System.Exception exception)
                {
                    Debug.LogError($"Cannot load scene: {MainScenePath}\n {exception.Message}");
                    EditorApplication.isPlaying = false;
                }
            }
            else
            {
                EditorApplication.isPlaying = false;
            }
        }

        if (!EditorApplication.isPlaying && !EditorApplication.isPlayingOrWillChangePlaymode)
        {
            var path = PlayerPrefs.GetString(PreviousScenePathKey);

            try
            {
                EditorSceneManager.OpenScene(path);
            }
            catch (System.Exception exception)
            {
                Debug.LogError($"Cannot load scene: {path}\n {exception.Message}");
            }
        }
    }
}
