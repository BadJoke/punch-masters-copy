﻿using Enemy.Humanoid;
using Enemy.Humanoid.Tools;
using UnityEditor;
using UnityEngine;

namespace Tools
{
    [CustomEditor(typeof(Ragdoll))]
    public class BonesDataManagerEditor : Editor
    {
        private float _liftingHeight;

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            _liftingHeight = EditorGUILayout.FloatField("Lifting height", _liftingHeight);

            if (GUILayout.Button("Save"))
            {
                BonesDataManager.Save(_liftingHeight, (Ragdoll)target);
            }
        }
    }
}
