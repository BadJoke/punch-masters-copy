﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class EndPanel : MonoBehaviour
    {
        [SerializeField] private float _showingTime = 0.5f;
        [SerializeField] private float _messsageHeightOffset = -70f;
        [SerializeField] private Image _background;
        [SerializeField] private Image _darker;
        [SerializeField] private Text _button;
        [SerializeField] private Text _message;

        private LevelManager _levelManager;

        private const string WinMessage = "COMPLETED!";
        private const string WinButtonText = "NEXT";
        private const string LoseMessage = "FAILED!";
        private const string LoseButtonText = "RESTART";

        public void Show(bool hasWin)
        {
            _levelManager = LevelManager.Instance;

            gameObject.SetActive(true);

            if (hasWin)
            {
                SetValues(WinMessage, WinButtonText);

                _levelManager.SaveProgress();
            }
            else
            {
                SetValues(LoseMessage, LoseButtonText);

                _background.DOFade(150f / 256f, _showingTime);
            }

            _message.rectTransform.DOAnchorPosY(_messsageHeightOffset, _showingTime);

            DOTween.Sequence()
                .Append(_button.transform.parent.DOScale(1.2f, _showingTime * 0.8f))
                .Append(_button.transform.parent.DOScale(1f, _showingTime * 0.2f));
        }

        private void SetValues(string message, string buttonText)
        {
            _message.text = message;
            _button.text = buttonText;
        }

        public void OnButtonClick()
        {
            _button.transform.parent.DOScale(0f, 0.05f);
            _darker.DOFade(1f, 0.3f).onComplete += () =>
            {
                _levelManager.LoadLevel();
            };
        }
    }
}