﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.StartMenu
{
    [RequireComponent(typeof(Image))]
    public class TapEffect : MonoBehaviour
    {
        [SerializeField] private RectTransform _zone;
        [SerializeField] private Image _pointerImage;
        [SerializeField] private bool _canRandomChangePosition;

        private RectTransform _tapRect;
        private Image _tapImage;
        private TapPositionChanger _changer;

        private void Start()
        {
            _tapImage = GetComponent<Image>();
            _tapRect = _tapImage.rectTransform;

            _changer = new TapPositionChanger(_zone);

            if (_canRandomChangePosition)
            {
                RandomPositionChanging();
            }
        }

        private void RandomPositionChanging()
        {
            transform.localPosition = _changer.RandomPosition();
        }
    }

}