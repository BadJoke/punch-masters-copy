﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UI.StartMenu
{
    public class Menu : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private float _disablingTime = 0.15f;
        [SerializeField] private RectTransform _message;

        public event UnityAction LevelStarted;

        public void OnPointerDown(PointerEventData eventData)
        {
            _message.DOScaleX(0f, _disablingTime).onComplete += () =>
            {
                gameObject.SetActive(false);

                LevelStarted?.Invoke();
            };
        }
    }
}
