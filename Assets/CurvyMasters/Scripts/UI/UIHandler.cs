﻿using UI.StartMenu;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIHandler : MonoBehaviour
    {
        [SerializeField] private Text _level;
        [SerializeField] private Menu _menu;
        [SerializeField] private RestartButton _restartButton;
        [SerializeField] private EndPanel _endPanel;
        [SerializeField] private EndDeterminator _determinator;

        private void OnEnable()
        {
            _menu.LevelStarted += OnLevelStarted;
            _determinator.LevelEnded += OnLevelEnded;
        }

        private void Start()
        {
            _level.text = "LEVEL " + LevelManager.Instance.CurrentLevel;
        }

        private void OnLevelStarted()
        {
            _menu.LevelStarted -= OnLevelStarted;

            _restartButton.Show();
        }

        private void OnLevelEnded(bool hasWin)
        {
            _determinator.LevelEnded -= OnLevelEnded;

            _restartButton.Hide();
            _endPanel.Show(hasWin);
        }
    }
}
