﻿using Player;
using UnityEngine;
using UnityEngine.Events;

namespace Enemy
{
    [RequireComponent(typeof(IDestroyable), typeof(EnemyMovement))]
    public class Enemy : MonoBehaviour
    {
        [SerializeField] private PlayerController _player;

        private IDestroyable _agent;
        private EnemyMovement _movement;

        public event UnityAction<Enemy> Died;
        
        private void Awake()
        {
            _agent = GetComponent<IDestroyable>();
            _agent.Destroyed += OnAgentDestroyed;

            _movement = GetComponent<EnemyMovement>();
        }

        public void GoToPlayer(PlayerController player)
        {
            _player = player;
            _player.Died += OnPlayerDied;
            _movement.GoTo(_player.transform);
        }

        private void Attack()
        {
            _player.Died -= OnPlayerDied;

            _player.TakeHit();
        }

        private void OnAgentDestroyed()
        {
            _agent.Destroyed -= OnAgentDestroyed;

            _movement.enabled = false;

            Died?.Invoke(this);
        }

        private void OnPlayerDied()
        {
            _player.Died -= OnPlayerDied;

            _movement.Stop();
        }
    }
}
