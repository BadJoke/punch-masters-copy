﻿using System;

namespace Enemy
{
    [Serializable]
    public struct EnemyData
    {
        public EnemyType Type;
        public Enemy Prefab;
    }
}
