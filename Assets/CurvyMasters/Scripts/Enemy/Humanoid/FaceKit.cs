﻿using System;
using UnityEngine;

namespace Enemy.Humanoid
{
    [Serializable]
    public class FaceKit
    {
        [SerializeField] private Material _normal;
        [SerializeField] private Material _dead;
        [SerializeField] private Material _hited;

        public Material Normal => _normal;
        public Material Dead => _dead;
        public Material Hited => _hited;
    }
}
