﻿using Enemy.Humanoid.Bones;
using Enemy.Humanoid.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Enemy.Humanoid
{
    public class RagdollLifter : MonoBehaviour
    {
        [SerializeField] private Transform _hips;
        [SerializeField] private Rigidbody _liftingRigidbody;

        [Space]
        [SerializeField, Range(0.5f, 1f)] private float _heightLiftThreshold = 1f;
        [SerializeField] private float _liftForce = 100f;
        [SerializeField] private float _bonesPlacingSpeed = 4f;

        private float _liftingHeight;
        private Vector3 _hipsPosition;
        private Transform _liftingBone;
        private List<Transform> _bones;
        private List<Vector3> _bonesPositions;
        private List<Quaternion> _bonesRotations;
        private Coroutine _lifting;
        private Coroutine _bonesSetting;

        public event UnityAction Lifted;

        private void Awake()
        {
            LoadBonesData();

            _liftingBone = _liftingRigidbody.transform;
        }

        public void Init(Bone[] bones)
        {
            _bones = new List<Transform>();

            foreach (var bone in bones)
            {
                _bones.Add(bone.transform);
            }
        }

        public void Lift()
        {
            ResetObjectPosition();

            var positionToLift = _liftingBone.position;
            positionToLift.y = _liftingHeight;
            _lifting = StartCoroutine(Lifting(positionToLift));
        }

        public void Stop()
        {
            if (_lifting != null)
            {
                StopCoroutine(_lifting);
            }

            if (_bonesSetting != null)
            {
                StopCoroutine(_bonesSetting);
            }
        }

        private IEnumerator Lifting(Vector3 liftingPosition)
        {
            var direction = (liftingPosition - _liftingBone.position).normalized;
            var targetHeight = _liftingHeight * _heightLiftThreshold;

            while (_liftingBone.position.y < targetHeight)
            {
                yield return new WaitForFixedUpdate();

                _liftingRigidbody.velocity = direction * _liftForce * Time.deltaTime;
            }

            _bonesSetting = StartCoroutine(BonesPositionsSetting());
        }

        private IEnumerator BonesPositionsSetting()
        {
            if (_bones.Count < 1 || _bonesPositions.Count < 1 || _bonesRotations.Count < 1)
            {
                throw new ArgumentException("Not enough bones data");
            }

            var bonesPositions = new List<Vector3>();
            var bonesRotations = new List<Quaternion>();

            foreach (Transform bone in _bones)
            {
                bonesPositions.Add(bone.localPosition);
                bonesRotations.Add(bone.localRotation);
            }

            float progress = 0f;

            while (progress < 1f)
            {
                for (int i = 0; i < _bones.Count; i++)
                {
                    _bones[i].localPosition = Vector3.Slerp(bonesPositions[i], _bonesPositions[i], progress);
                    _bones[i].localRotation = Quaternion.Slerp(bonesRotations[i], _bonesRotations[i], progress);
                }

                progress += _bonesPlacingSpeed * Time.deltaTime;

                yield return null;
            }

            Lifted?.Invoke();
        }

        private void ResetObjectPosition()
        {
            _hipsPosition = _hips.position;

            var newPosition = _hipsPosition;
            newPosition.y = transform.position.y;

            transform.position = newPosition;
            _hips.position = _hipsPosition;
        }

        private void LoadBonesData()
        {
            var data = BonesDataManager.Load();

            _liftingHeight = data.LiftingHeight;
            _bonesPositions = data.Positions;
            _bonesRotations = data.Rotations;
        }
    }
}