﻿using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Humanoid.Tools
{
    public class BonesData : ScriptableObject
    {
        public float LiftingHeight;
        public List<Vector3> Positions;
        public List<Quaternion> Rotations;
    }
}
