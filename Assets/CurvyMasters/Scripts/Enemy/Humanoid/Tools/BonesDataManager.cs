﻿using Enemy.Humanoid.Bones;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Enemy.Humanoid.Tools
{
    public static class BonesDataManager
    {
        private const string Path = "Assets/CurvyMasters/Resources/BonesData.asset";
        private const string Name = "BonesData";

#if UNITY_EDITOR
        public static void Save(float height, Ragdoll ragdoll)
        {
            var bones = ragdoll.GetComponentsInChildren<Bone>();
            List<Vector3> positions = new List<Vector3>();
            List<Quaternion> rotations = new List<Quaternion>();

            foreach (var bone in bones)
            {
                positions.Add(bone.transform.localPosition);
                rotations.Add(bone.transform.localRotation);
            }

            BonesData data = CreateData(height, positions, rotations);

            AssetDatabase.CreateAsset(data, Path);
            AssetDatabase.SaveAssets();
        }
#endif

        public static BonesData Load()
        {
            return Resources.Load<BonesData>(Name);
        }

        private static BonesData CreateData(float height, List<Vector3> positions, List<Quaternion> rotations)
        {
            var data = ScriptableObject.CreateInstance<BonesData>();
            data.LiftingHeight = height;
            data.Positions = positions;
            data.Rotations = rotations;
            return data;
        }
    }
}