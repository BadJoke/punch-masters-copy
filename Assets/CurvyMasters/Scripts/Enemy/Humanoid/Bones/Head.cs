﻿using Player.Punching;
using UnityEngine;

namespace Enemy.Humanoid.Bones
{
    public class Head : Bone
    {
        [SerializeField] private Rigidbody _spine;
        [SerializeField] private Vector3 _force;

        public override void TakeDamage(DamageData damageData)
        {
            Push(damageData);

            _spine.AddForce(_force);
        }
    }
}
