﻿using Player.Punching;
using UnityEngine.Events;

namespace Enemy.Humanoid.Bones
{
    public class Body : Bone
    {
        public event UnityAction<Body> Punched;

        public override void TakeDamage(DamageData damageData)
        {
            ApplyDamage(damageData.Damage);
            PushBody(damageData);
            Punched?.Invoke(this);
        }
    }
}
