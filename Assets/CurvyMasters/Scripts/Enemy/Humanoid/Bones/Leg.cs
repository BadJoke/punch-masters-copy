﻿using Player.Punching;

namespace Enemy.Humanoid.Bones
{
    public class Leg : Bone
    {
        public override void TakeDamage(DamageData damageData)
        {
            Push(damageData);
        }
    }
}
