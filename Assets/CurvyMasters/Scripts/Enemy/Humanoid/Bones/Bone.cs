﻿using Player.Punching;
using UnityEngine;
using UnityEngine.Events;

namespace Enemy.Humanoid.Bones
{
    [RequireComponent(typeof(Rigidbody))]
    public abstract class Bone : MonoBehaviour, IDamagable
    {
        [SerializeField, Range(1, 15)] private int _damageModifier = 1;

        private Rigidbody _rigidbody;

        public event UnityAction<int> Damaged;
        public event UnityAction<int> Pushed;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void SetActive(bool isActive)
        {
            _rigidbody.useGravity = isActive;
            _rigidbody.isKinematic = !isActive;
        }

        public abstract void TakeDamage(DamageData damageData);

        protected void ApplyDamage(int damage)
        {
            Damaged?.Invoke(damage * _damageModifier);
        }

        protected void PushBody(DamageData damageData)
        {
            Push(damageData.Force, damageData.Position);

            _rigidbody.AddForce(Vector3.up * damageData.Force * 3f);
        }

        protected void Push(DamageData damageData)
        {
            Pushed?.Invoke(damageData.Damage * _damageModifier);

            Push(damageData.Force, damageData.Position);
        }

        private void Push(float force, Vector3 from)
        {
            var punchDirection = transform.position - from;
            punchDirection.y = 0;
            punchDirection.Normalize();

            _rigidbody.AddForce(punchDirection * force);
        }
    }
}
