﻿using Player.Punching;

namespace Enemy.Humanoid.Bones
{
    public class Arm : Bone
    {
        public override void TakeDamage(DamageData damageData)
        {
            ApplyDamage(damageData.Damage);
        }
    }
}
