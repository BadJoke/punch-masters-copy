﻿using UnityEngine;

namespace Enemy.Humanoid
{
    public class HumanoidAnimator : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private Ragdoll _ragdoll;
        [SerializeField] private RagdollLifter _lifter;
        [SerializeField] private EnemyMovement _movement;
        
        private bool _isAlive = true;
        private Appearance _appearance;

        private const string IsIdle2Bool = "IsIdle2";
        private const string ActiveBool = "IsActive";
        private const string HitTrigger = "Hit";
        private const string AttackTrigger = "Attack";
        private const string WalkAnimation = "Walk";

        private void OnEnable()
        {
            _ragdoll.Hitted += OnHitted;
            _ragdoll.Ragdolled += OnRagdolled;
            _ragdoll.Destroyed += OnDestroyed;
            _lifter.Lifted += OnLifted;

            _animator.SetBool(IsIdle2Bool, Random.value < 0.5f);
        }

        private void Start()
        {
            _appearance = GetComponent<Appearance>();

            _movement.Moving += OnMoving;
            _movement.Moved += OnMoved;
        }

        private void OnDisable()
        {
            _ragdoll.Hitted -= OnHitted;
            _ragdoll.Ragdolled -= OnRagdolled;
            _ragdoll.Destroyed -= OnDestroyed;
            _lifter.Lifted -= OnLifted;
        }

        public void HitEnded()
        {
            _movement.IsHitted = false;
        }

        private void OnHitted()
        {
            if (_isAlive)
            {
                _appearance?.Hit();
            }

            if (_movement.IsHitted == false)
            {
                _movement.IsHitted = true;

                _animator.SetTrigger(HitTrigger);
            }
        }

        private void OnRagdolled()
        {
            if (_isAlive)
            {
                _movement.IsHitted = true;
                _appearance?.Hit();
            }

            _movement.enabled = false;
            _animator.enabled = false;
        }

        private void OnLifted()
        {
            if (_isAlive)
            {
                _appearance?.RestoreFace();

                _movement.IsHitted = false;
                _movement.enabled = true;
                _animator.ResetTrigger(HitTrigger);
                _animator.Play(WalkAnimation, 0, 0f);
                _animator.enabled = true;
            }
        }

        private void OnMoving()
        {
            _movement.Moving -= OnMoving;

            _animator.SetBool(ActiveBool, true);
        }

        private void OnMoved(bool isPlayerRecahed)
        {
            _movement.Moved -= OnMoved;

            if (isPlayerRecahed)
            {
                _animator.SetTrigger(AttackTrigger);
            }

            _animator.SetBool(ActiveBool, false);
        }

        private void OnDestroyed()
        {
            _isAlive = false;

            _animator.enabled = false;
            _appearance?.Die();
            _lifter.Stop();
        }
    }
}