﻿using Enemy.Humanoid.Bones;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Enemy.Humanoid
{
    [RequireComponent(typeof(RagdollLifter))]
    public class Ragdoll : MonoBehaviour, IDestroyable
    {
        [SerializeField] private int _health = 5;
        [SerializeField] private float _liftingDelay = 2f;

        private Bone[] _bones;
        private List<Body> _bodies;
        private RagdollLifter _lifter;

        public event UnityAction Hitted;
        public event UnityAction Ragdolled;
        public event UnityAction Destroyed;

        private void Start()
        {
            _bones = GetComponentsInChildren<Bone>();
            _bodies = new List<Body>();

            foreach (var bone in _bones)
            {
                bone.Damaged += OnBoneDamaged;

                if (bone is Body body)
                {
                    body.Punched += OnBodyPunched;
                    _bodies.Add(body);
                }
                else
                {
                    bone.Pushed += OnBonePushed;
                }
            }

            _lifter = GetComponent<RagdollLifter>();
            _lifter.Init(_bones);
            _lifter.Lifted += OnLifted;
        }

        private void Activate()
        {
            _lifter.Stop();
            Ragdolled?.Invoke();

            CancelInvoke();
            SetActive(true);
        }

        private void Deactivate()
        {
            SetActive(false);
        }

        private void SetActive(bool isActive)
        {
            foreach (var bone in _bones)
            {
                bone.SetActive(isActive);
            }
        }

        private void Lifting()
        {
            _lifter.Lift();
        }

        private void ApplyDamage(int damage)
        {
            _health -= damage;

            if (_health < 1)
            {
                Destroy();
            }
        }

        private void Destroy()
        {
            Activate();

            foreach (var bone in _bones)
            {
                bone.Damaged -= OnBoneDamaged;
                bone.Pushed -= OnBonePushed;
            }

            foreach (var body in _bodies)
            {
                body.Punched -= OnBodyPunched;
            }

            Destroyed?.Invoke();
        }

        private void OnBoneDamaged(int damage)
        {
            ApplyDamage(damage);
        }

        private void OnBonePushed(int damage)
        {
            ApplyDamage(damage);
            Activate();

            if (_health > 0)
            {
                Invoke(nameof(Lifting), _liftingDelay);
            }
        }

        private void OnBodyPunched(Body body)
        {
            if (_health > 0)
            {
                Hitted?.Invoke();
            }
        }

        private void OnLifted()
        {
            Deactivate();
        }
    }
}
