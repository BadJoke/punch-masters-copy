﻿using System;
using UnityEngine;

namespace Enemy.Humanoid
{
    [Serializable]
    public class CapKit
    {
        [SerializeField] private Material _main;
        [SerializeField] private Material _visor;

        public Material Main => _main;
        public Material Visor => _visor;
    }
}
