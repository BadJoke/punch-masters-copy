﻿using UnityEngine;

namespace Enemy.Humanoid
{
    public class Appearance : MonoBehaviour
    {
        [SerializeField] private MeshRenderer _faceRenderer;
        [SerializeField] private MeshRenderer _capRenderer;
        [SerializeField] private MeshRenderer[] _shoesRenderer;
        [SerializeField] private SkinnedMeshRenderer _skinRenderer;
        [SerializeField] private FaceKit[] _faces;
        [SerializeField] private CapKit[] _caps;
        [SerializeField] private Material[] _shoes;
        [SerializeField] private Material[] _shirts;
        [SerializeField] private Material[] _pants;
        [SerializeField] private Material[] _hairs;

        private FaceKit _face;

        private const int ShirtIndex = 0;
        private const int PantsIndex = 1;
        private const int HairIndex = 4;

        private void Start()
        {
            SetAppearance();
        }

        public void RestoreFace()
        {
            _faceRenderer.material = _face.Normal;
        }

        public void Hit()
        {
            _faceRenderer.material = _face.Hited;
        }

        public void Die()
        {
            _faceRenderer.material = _face.Dead;

            var skinMaterials = _skinRenderer.materials;
            var grayMaterials = new Material[skinMaterials.Length];

            for (int i = 0; i < grayMaterials.Length; i++)
            {
                var color = skinMaterials[i].color;
                var average = color.grayscale;
                color = new Color(average, average, average, 1.0f);

                grayMaterials[i] = new Material(skinMaterials[i]);
                grayMaterials[i].color = color;
            }

            _skinRenderer.materials = grayMaterials;
        }

        private void SetAppearance()
        {
            _face = _faces[Random.Range(0, _faces.Length)];
            _faceRenderer.material = _face.Normal;

            var cap = _caps[Random.Range(0, _caps.Length)];
            var capMaterials = _capRenderer.materials;
            capMaterials[0] = new Material(cap.Main);
            capMaterials[1] = new Material(cap.Visor);
            _capRenderer.materials = capMaterials;

            var shoes = _shoes[Random.Range(0, _shoes.Length)];

            foreach (var shoe in _shoesRenderer)
            {
                shoe.material = new Material(shoes);
            }

            var skinMaterials = _skinRenderer.materials;
            skinMaterials[ShirtIndex] = new Material(_shirts[Random.Range(0, _shirts.Length)]);
            skinMaterials[PantsIndex] = new Material(_pants[Random.Range(0, _pants.Length)]);
            skinMaterials[HairIndex] = new Material(_hairs[Random.Range(0, _hairs.Length)]);
            _skinRenderer.materials = skinMaterials;
        }
    }
}