﻿using UnityEngine;

namespace Enemy.Humanoid
{
    [RequireComponent(typeof(Enemy))]
    public class ThingsDetacher : MonoBehaviour
    {
        [SerializeField] private Vector3 _detachForce;
        [SerializeField] private Thing _cap;
        [SerializeField] private Thing[] _shoes;
        [SerializeField] private Thing[] _weapons;

        private Thing _weapon;
        private IDestroyable _agent;

        private void Awake()
        {
            if (_weapons.Length > 0)
            {
                _weapon = _weapons[Random.Range(0, _weapons.Length)];
                _weapon.gameObject.SetActive(true);
            }

            _agent = GetComponent<IDestroyable>();
            _agent.Destroyed += OnAgentDestroyed;
        }

        private void OnAgentDestroyed()
        {
            _agent.Destroyed -= OnAgentDestroyed;

            _cap?.Detach();
            _weapon.Detach();

            foreach (var boot in _shoes)
            {
                boot.Detach(_detachForce);
            }
        }
    }
}