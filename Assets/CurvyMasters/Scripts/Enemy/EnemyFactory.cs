﻿using UnityEngine;

namespace Enemy
{
    public class EnemyFactory
    {
        private Enemy _prefab;
        private Transform _container;

        public EnemyFactory(Enemy prefab, Transform container)
        {
            _prefab = prefab;
            _container = container;
        }

        public Enemy Create(Vector3 position)
        {
            return Object.Instantiate(_prefab, position, Quaternion.identity, _container);
        }
    }
}
