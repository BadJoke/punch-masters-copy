﻿using Player.Punching;

namespace Enemy
{
    public interface IDamagable
    {
        void TakeDamage(DamageData damageData);
    }
}
