﻿using UnityEngine;

namespace Enemy
{
    [RequireComponent(typeof(Collider), typeof(Rigidbody))]
    public class Thing : MonoBehaviour
    {
        private Collider _collider;
        private Rigidbody _rigidbody;

        private void Awake()
        {
            _collider = GetComponent<Collider>();
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void Detach()
        {
            _collider.enabled = true;
            _rigidbody.isKinematic = false;
            _rigidbody.useGravity = true;

            transform.SetParent(null);
        }

        public void Detach(Vector3 force)
        {
            Detach();

            _rigidbody.AddForce(force);
        }
    }
}
