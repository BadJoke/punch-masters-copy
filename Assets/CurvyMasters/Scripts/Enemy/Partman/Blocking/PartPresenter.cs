﻿using UnityEngine;

namespace Enemy.Partman.Blocking
{
    public class PartPresenter : MonoBehaviour
    {
        [SerializeField] private ParticleSystem _crumblingEffect;

        private Color _originalColor;
        private Color _originalColorEmission;

        private const float ParticleSpeedModifier = 5f;
        private const string ColorName = "_Color";
        private const string EmissionName = "_Emission";

        private void Start()
        {
            SetColors();
        }

        private void SetColors()
        {
            var material = GetComponent<MeshRenderer>().material;
            _originalColor = material.GetColor(ColorName);
            _originalColorEmission = material.GetColor(EmissionName);
        }

        public void BlockDamaged()
        {
            Destroy(1f, false);
        }

        public void BlockDestroyed()
        {
            Destroy(1f, true);
        }

        public void BlockFullDestroyed()
        {
            Destroy(ParticleSpeedModifier, true);
        }

        private void Destroy(float particleSpeedModifier, bool isDead)
        {
            var effect = Instantiate(_crumblingEffect);
            effect.transform.position = transform.position;

            var renderer = effect.GetComponent<ParticleSystemRenderer>();
            renderer.material.SetColor(ColorName, _originalColor);
            renderer.material.SetColor(EmissionName, _originalColorEmission);

            var main = effect.main;
            main.startSpeedMultiplier = particleSpeedModifier;

            effect.Play();

            if (isDead)
            {
                gameObject.SetActive(false);
            }
        }
    }
}