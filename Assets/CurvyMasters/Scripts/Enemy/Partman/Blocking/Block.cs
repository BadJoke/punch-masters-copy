﻿using Player.Punching;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Enemy.Partman.Blocking
{
    [RequireComponent(typeof(Collider))]
    public class Block : MonoBehaviour, IDamagable
    {
        [SerializeField] private PartPresenter[] _parts;

        public event UnityAction<Block> Damaged;

        public void TakeDamage(DamageData damageData)
        {
            Damaged?.Invoke(this);
        }

        public void Damage()
        {
            var destroyCount = _parts.Length / 2;
            var parts = new List<PartPresenter>(_parts);
            var destroyedParts = new List<PartPresenter>();

            for (int i = 0; i < destroyCount; i++)
            {
                var part = parts[Random.Range(0, parts.Count)];
                parts.Remove(part);
                destroyedParts.Add(part);
            }

            foreach (var part in parts)
            {
                part.BlockDamaged();
            }

            foreach (var part in destroyedParts)
            {
                part.BlockDestroyed();
            }
        }

        public void Destroy()
        {
            foreach (var part in _parts)
            {
                part.BlockDestroyed();
            }

            gameObject.SetActive(false);
        }

        public void FullDestroy()
        {
            foreach (var part in _parts)
            {
                part.BlockFullDestroyed();
            }
        }
    }
}