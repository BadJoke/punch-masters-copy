﻿using UnityEngine;

namespace Enemy.Partman
{
    public class PartmanAnimator : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private PartmanController _controller;
        [SerializeField] private EnemyMovement _movement;

        private const string IsIdle2Bool = "IsIdle2";
        private const string IsActiveBool = "IsActive";
        private const string IsTwoLeggedBool = "IsTwoLegged";
        private const string HitTrigger = "Hit";
        private const string AttackTrigger = "Attack";

        private void OnEnable()
        {
            _animator.SetBool(IsIdle2Bool, Random.value < 0.5f);
        }

        private void Start()
        {
            _controller.Hitted += OnHitted;
            _controller.LegDestroyed += OnLegDestroyed;
            _controller.LegsDestroyed += OnLegsDestroyed;
            _movement.Moving += OnMoving;
            _movement.Moved += OnMoved;
        }

        private void HitEnded()
        {
            _movement.IsHitted = false;
        }

        private void OnHitted()
        {
            _controller.Hitted -= OnHitted;

            if (_movement.IsHitted == false)
            {
                _movement.IsHitted = true;

                _animator.SetTrigger(HitTrigger);
            }
        }

        private void OnLegDestroyed()
        {
            _animator.SetBool(IsTwoLeggedBool, false);
        }

        private void OnLegsDestroyed()
        {
            _movement.IsHitted = true;
            _animator.enabled = false;
        }

        private void OnMoving()
        {
            _movement.Moving -= OnMoving;

            _animator.SetBool(IsActiveBool, true);
        }

        private void OnMoved(bool isPlayerRecahed)
        {
            _movement.Moved -= OnMoved;

            if (isPlayerRecahed)
            {
                _animator.SetTrigger(AttackTrigger);
            }

            _animator.SetBool(IsActiveBool, false);
        }
    }
}
