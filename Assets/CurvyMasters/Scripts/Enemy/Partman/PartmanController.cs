﻿using DG.Tweening;
using Enemy.Partman.Blocking;
using UnityEngine;
using UnityEngine.Events;

namespace Enemy.Partman
{
    public partial class PartmanController : MonoBehaviour, IDestroyable
    {
        [SerializeField] private Block _rightLeg;
        [SerializeField] private Block _leftLeg;
        [SerializeField] private Block _head;
        [SerializeField] private Block _body;
        [SerializeField] private PartPresenter[] _arms;
        [SerializeField] private Rigidbody[] _glows;
        [SerializeField] private float _force;

        private PartmanCondition _condition;
        private bool _isFirstBodyHit;

        public event UnityAction Hitted;
        public event UnityAction LegDestroyed;
        public event UnityAction LegsDestroyed;
        public event UnityAction Destroyed;

        private void Start()
        {
            _condition = PartmanCondition.TwoLegged;
            _isFirstBodyHit = true;

            _head.Damaged += OnHeadDamaged;
            _body.Damaged += OnBodyDamaged;
            _rightLeg.Damaged += OnLegDamaged;
            _leftLeg.Damaged += OnLegDamaged;
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.layer == 10)
            {
                _head.FullDestroy();
                _body.FullDestroy();
            }
        }

        private void DisableLeg(Block block)
        {
            if (block == _rightLeg)
            {
                _rightLeg.Destroy();
                _rightLeg.Damaged -= OnLegDamaged;
                _rightLeg = null;
            }
            else
            {
                _leftLeg.Destroy();
                _leftLeg.Damaged -= OnLegDamaged;
                _leftLeg = null;
            }
        }

        private void Destroy()
        {
            _head?.FullDestroy();
            _body?.FullDestroy();
            _rightLeg?.FullDestroy();
            _leftLeg?.FullDestroy();

            foreach (var part in _arms)
            {
                part.BlockFullDestroyed();
            }

            foreach (var glow in _glows)
            {
                DetachGlow(glow);
            }

            Destroyed?.Invoke();

            gameObject.SetActive(false);
        }

        private void DetachGlow(Rigidbody glow)
        {
            var direction = glow.position - transform.position;
            direction.y = 0;
            var force = direction.normalized * _force;

            glow.transform.SetParent(null);
            glow.isKinematic = false;
            glow.useGravity = true;
            glow.AddForce(force);
            glow.AddTorque(force);

            glow.GetComponent<Collider>().enabled = true;
        }

        private void OnHeadDamaged(Block block)
        {
            _head.Damaged -= OnHeadDamaged;
            _head = null;

            Destroy();
        }

        private void OnBodyDamaged(Block block)
        {
            if (_isFirstBodyHit && _condition != PartmanCondition.LegLess)
            {
                _isFirstBodyHit = false;
                _body.Damage();
                Hitted?.Invoke();
            }
            else
            {
                _body.Damaged -= OnBodyDamaged;
                _body = null;

                Destroy();
            }
        }

        private void OnLegDamaged(Block block)
        {
            switch (_condition)
            {
                case PartmanCondition.TwoLegged:
                    DisableLeg(block);

                    LegDestroyed?.Invoke();

                    _condition = PartmanCondition.OneLegged;
                    break;
                case PartmanCondition.OneLegged:
                    DisableLeg(block);

                    LegsDestroyed?.Invoke();

                    _body.transform
                        .DOLocalMoveY(0f, 0.75f)
                        .SetEase(Ease.InQuad)
                        .onComplete += () =>
                        {
                            Destroy();
                        };

                    _condition = PartmanCondition.LegLess;
                    break;
            }
        }
    }
}
