﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace Enemy
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class EnemyMovement : MonoBehaviour
    {
        [SerializeField] private float _speed = 1.5f;
        [SerializeField] private float _playerOffset = 3f;

        public bool IsHitted = false;

        private Transform _target;
        private NavMeshAgent _navMeshAgent;

        public event UnityAction Moving;
        public event UnityAction<bool> Moved;

        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        private void Update()
        {
            if (IsHitted)
            {
                return;
            }

            transform.LookAt(_target);
            _navMeshAgent.Move(_speed * Time.deltaTime * transform.forward);

            if (Vector3.Distance(transform.position, _target.position) < _playerOffset)
            {
                enabled = false;

                Moved?.Invoke(true);
            }
        }

        public void GoTo(Transform target)
        {
            _target = target;

            enabled = true;

            Moving?.Invoke();
        }

        public void Stop()
        {
            enabled = false;

            Moved?.Invoke(false);
        }
    }
}
