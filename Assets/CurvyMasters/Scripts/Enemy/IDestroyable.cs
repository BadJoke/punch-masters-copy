﻿using UnityEngine.Events;

namespace Enemy
{
    public interface IDestroyable
    {
        event UnityAction Destroyed;
    }
}
