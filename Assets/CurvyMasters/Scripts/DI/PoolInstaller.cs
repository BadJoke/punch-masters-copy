﻿using FUGames.Pooling;
using UnityEngine;
using Zenject;

namespace DI
{
    public class PoolInstaller : MonoInstaller
    {
        [SerializeField] private PoolManager _poolManager;

        public override void InstallBindings()
        {
            Container
                .Bind<PoolManager>()
                .FromInstance(_poolManager)
                .AsSingle();
        }
    }
}
