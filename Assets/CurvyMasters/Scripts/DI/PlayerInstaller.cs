﻿using Player;
using UnityEngine;
using Waypoints;
using Zenject;

namespace DI
{
    public class PlayerInstaller : MonoInstaller
    {
        [SerializeField] private Waypoint _spawnPoint;
        [SerializeField] private PlayerController _playerPrefab;

        public override void InstallBindings()
        {
            Container
                .Bind<Waypoint>()
                .FromInstance(_spawnPoint)
                .AsSingle();

            var player = Container
                .InstantiatePrefabForComponent<PlayerController>(
                _playerPrefab, 
                _spawnPoint.transform.position, 
                Quaternion.identity, 
                null);

            Container
                .Bind<PlayerController>()
                .FromInstance(player)
                .AsSingle();
        }
    }
}
