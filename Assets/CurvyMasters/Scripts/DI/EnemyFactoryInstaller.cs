﻿using Enemy;
using System.Linq;
using UnityEngine;
using Zenject;

namespace DI
{
    public class EnemyFactoryInstaller : MonoInstaller
    {
        public EnemyData[] Enemies;
        public EnemyType Type;
        public Transform EnemyContainer;

        public override void InstallBindings()
        {
            var prefab = Enemies.First(enemy => enemy.Type == Type).Prefab;
            var factory = new EnemyFactory(prefab, EnemyContainer);

            Container
                .Bind<EnemyFactory>()
                .FromInstance(factory)
                .AsSingle();
        }
    }
}
