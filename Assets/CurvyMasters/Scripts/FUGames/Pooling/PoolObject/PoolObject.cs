﻿using UnityEngine;

namespace FUGames.Pooling
{ 
    public abstract class PoolObject : MonoBehaviour
    {
        private PoolManager _manager;

        public virtual void Init(PoolObjectData data) { }

        public virtual void BackToPool()
        {
            _manager.Put(this);
        }

        public void SetManager(PoolManager manager)
        {
            _manager = manager;
        }
    }
}