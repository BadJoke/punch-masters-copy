﻿using Player;
using UI.StartMenu;
using UnityEngine;
using UnityEngine.Events;
using Waypoints;
using Zenject;

public class EndDeterminator : MonoBehaviour
{
    [SerializeField] private Menu _menu;
    [SerializeField] private PlayerController _player;
    [SerializeField] private Waypoint _finalPoint;
    [SerializeField] private Transform[] _confettiPoints;
    [SerializeField] private ParticleSystem _confettiPrefab;

    public event UnityAction<bool> LevelEnded;

    [Inject]
    private void Constructor(PlayerController player)
    {
        _player = player;
    }

    private void Awake()
    {
        _menu.LevelStarted += OnLevelStarted;
        _player.Died += OnPlayerDied;
        _finalPoint.Reached += OnFinalPointReached;
    }

    private void RemoveListeners()
    {
        _player.Died -= OnPlayerDied;
        _finalPoint.Reached -= OnFinalPointReached;
    }

    private void OnLevelStarted()
    {
        _menu.LevelStarted -= OnLevelStarted;

        _player.StartLevel();
    }

    private void OnPlayerDied()
    {
        RemoveListeners();

        LevelEnded?.Invoke(false);
    }

    private void OnFinalPointReached()
    {
        RemoveListeners();

        foreach (var point in _confettiPoints)
        {
            Instantiate(_confettiPrefab, point.position, Quaternion.Euler(-90f, 0f, 0f));
        }

        LevelEnded?.Invoke(true);
    }
}
