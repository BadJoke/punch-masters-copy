﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class CurveMeshGenerator : MonoBehaviour
{
    [SerializeField] private float _radius = 1f;
    [SerializeField] private int _density = 8;
    [SerializeField] private Transform _rotationAssistant;

    private Mesh _mesh;
    private List<Vector3> _vertices;
    private List<int> _triangles;
    private int _borderVerticeSize;
    private float _maxAngle = Mathf.PI * 2f;
    private float _angleStep;

    private void Awake()
    {
        _mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = _mesh;
    }

    public void Generate(List<CurvePoint> points)
    {
        _borderVerticeSize = _density + 1;
        _angleStep = _maxAngle / _density;
        _triangles = new List<int>();
        _vertices = new List<Vector3>();
        _rotationAssistant.localPosition = Vector3.zero;

        if (points.Count > 0)
        {
            GenerateFront(points[0].Position);
            GenerateBody(points);
            GenerateBackside(points[points.Count - 1].Position);
        }

        UpdateMesh();
    }

    public void Show(int length)
    {
        var verticesCount = length * _density + 2;

        _triangles.Clear();

        if (length > 0)
        {
            SetFrontTriangles();

            for (int i = _density * 2 + 1; i < verticesCount; i += _density)
            {
                SetBodyTriangles(i);
            }

            SetBacksideTriangles(verticesCount);
        }

        UpdateMesh();
    }

    private void GenerateFront(Vector3 bodyPoint)
    {
        _vertices.Add(bodyPoint);

        GenerateStep(bodyPoint);
        SetFrontTriangles();
    }

    private void GenerateBody(List<CurvePoint> points)
    {
        for (int i = 1; i < points.Count; i++)
        {
            GenerateStep(points[i].Position);
            SetBodyTriangles(_vertices.Count);
        }
    }

    private void GenerateBackside(Vector3 bodyPoint)
    {
        _vertices.Add(bodyPoint);

        SetBacksideTriangles(_vertices.Count);
    }

    private void GenerateStep(Vector3 bodyPoint)
    {
        var lookDirection = bodyPoint - _rotationAssistant.localPosition;

        for (int i = 0; i < _density; i++)
        {
            var angle = i * _angleStep;
            var point = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle)) * _radius;

            if (lookDirection != Vector3.zero)
            {
                point = Quaternion.LookRotation(lookDirection) * point;
            }

            _vertices.Add(point + bodyPoint);
        }

        _rotationAssistant.localPosition = bodyPoint;
    }

    private void SetFrontTriangles()
    {
        for (int i = 1; i < _borderVerticeSize; )
        {
            if (i == _borderVerticeSize - 1)
            {
                _triangles.AddRange(new int[] { i++, 0, 1 });
            }
            else
            {
                _triangles.AddRange(new int[] { i, 0, ++i });
            }
        }
    }
    
    private void SetBodyTriangles(int verticesCount)
    {
        var startVertex = verticesCount - _density;
        var finishVertex = verticesCount;

        for (int i = startVertex; i < finishVertex; i++)
        {
            if (i == finishVertex - 1)
            {
                _triangles.AddRange(new int[] { i, i - _density, startVertex });
                _triangles.AddRange(new int[] { i - _density, startVertex - _density, startVertex });
            }
            else
            {
                _triangles.AddRange(new int[] { i, i - _density, i + 1 });
                _triangles.AddRange(new int[] { i - _density, i - _density + 1, i + 1 });
            }
        }
    }

    private void SetBacksideTriangles(int verticesCount)
    {
        var lastIndex = verticesCount - 1;
        var startIndex = lastIndex - _density;

        for (int i = startIndex; i < lastIndex; )
        {
            if (i == lastIndex - 1)
            {
                _triangles.AddRange(new int[] { lastIndex, i++, startIndex });
            }
            else
            {
                _triangles.AddRange(new int[] { lastIndex, i, ++i });
            }
        }
    }

    private void UpdateMesh()
    {
        _mesh.Clear();

        _mesh.vertices = _vertices.ToArray();
        _mesh.triangles = _triangles.ToArray();

        _mesh.RecalculateNormals();
    }
}
