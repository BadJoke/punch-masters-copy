﻿using System.Collections.Generic;
using UnityEngine;

public class Curve
{
    private float _pointsDensity;
    private float _movingTime;
    private float _noiseAmplitude;
    private float _sinValue;
    private float _noiseOffset;
    private bool _isNewNoise;
    private Vector3 _start;
    private Vector3 _power;
    private List<float> _noise = new List<float>();
    private List<CurvePoint> _points = new List<CurvePoint>();

    public Curve(Vector3 start, Vector3 power, float movingTime)
    {
        _pointsDensity = 25f;
        _noiseAmplitude = 0.25f;
        _sinValue = Mathf.PI * 4f;
        _start = start;
        _power = power;
        _movingTime = movingTime;
    }

    public List<CurvePoint> Spawn(Vector3 end, float currentTime, bool hasNoise)
    {
        if (hasNoise)
        {
            if (_isNewNoise)
            {
                _sinValue = Mathf.PI * Random.Range(2, 9);
                _noiseOffset = Random.Range(0f, Mathf.PI * 2f);
                _isNewNoise = false;
            }

            _noise.Clear();
        }
        else
        {
            _isNewNoise = true;
        }

        _points.Clear();

        var centerOffset = ((_start + end) / 2f) + _power;
        var punchLength = Vector3.Distance(_start, centerOffset) + Vector3.Distance(centerOffset, end);
        var deltaTime = _movingTime / (punchLength * _pointsDensity);
        var amplitude = _noiseAmplitude * (1 - (currentTime / _movingTime));

        for (float time = 0; time < currentTime; time += deltaTime)
        {
            var position = Parabola.Parabola3D(_start, end, _power, time / _movingTime);

            if (hasNoise)
            {
                var seed = _noiseOffset + (time / _movingTime * _sinValue);
                _noise.Add(Mathf.Sin(seed) * amplitude);

                if (time + deltaTime < currentTime)
                {
                    position.x += _noise[_noise.Count - 1];
                }
            }

            var point = new CurvePoint(time, position);

            _points.Add(point);
        }
        
        return _points;
    }
    
    public void RotateFist(Transform fist)
    {
        var points = _points.Count;

        if (points < 2)
        {
            return;
        }

        var direction = _points[points - 1].Position - _points[points - 2].Position;
        fist.localRotation = Quaternion.LookRotation(direction);
    }
}