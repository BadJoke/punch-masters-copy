﻿using Enemy;
using Player.Punching;
using UnityEngine;

namespace Objects
{
    public class Box : MonoBehaviour, IDamagable
    {
        [SerializeField] private GameObject _fullBoxMesh;
        [SerializeField] private GameObject _brokenBoxMesh;

        public void TakeDamage(DamageData damageData)
        {
            Destroy();
        }

        private void Destroy()
        {
            _fullBoxMesh.SetActive(false);
            _brokenBoxMesh.SetActive(true);
        }
    }
}
