﻿using Enemy;
using Player.Punching;
using UnityEngine;

namespace Objects
{
    [RequireComponent(typeof(BoxCollider), typeof(Rigidbody))]
    public class DestroyablePart : MonoBehaviour, IDamagable
    {
        private ParticleSystem[] _crumblings;
        private Material[] _materials;

        private void Start()
        {
            _crumblings = GetComponentsInChildren<ParticleSystem>();
            _materials = GetComponent<MeshRenderer>().materials;
        }

        public void TakeDamage(DamageData damageData)
        {
            for (int i = 0; i < _crumblings.Length; i++)
            {
                _crumblings[i]
                    .GetComponent<ParticleSystemRenderer>()
                    .material
                    .CopyPropertiesFromMaterial(_materials[i]);

                _crumblings[i].transform.SetParent(null);
                _crumblings[i].Play();
            }

            gameObject.SetActive(false);
        }
    }
}
