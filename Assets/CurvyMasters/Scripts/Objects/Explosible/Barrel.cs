﻿using Enemy;
using Player.Punching;
using UnityEngine;

namespace Objects.Explosible
{
    [RequireComponent(typeof(Explosion))]
    public class Barrel : MonoBehaviour, IDamagable
    {
        [SerializeField] private GameObject _fullBarrelMesh;
        [SerializeField] private GameObject _brokenBarrelMesh;

        private Explosion _explosion;
        private void Start()
        {
            Init();
        }

        public void TakeDamage(DamageData damageData)
        {
            Destroy();
        }

        private void Init()
        {
            _explosion = GetComponent<Explosion>();
        }

        private void Destroy()
        {
            _fullBarrelMesh.SetActive(false);
            _brokenBarrelMesh.SetActive(true);

            _explosion.Explode();
        }
    }
}
