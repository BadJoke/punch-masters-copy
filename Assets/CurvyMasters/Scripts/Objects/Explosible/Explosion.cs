﻿using Enemy;
using Enemy.Humanoid.Bones;
using Player.Punching;
using UnityEngine;

namespace Objects.Explosible
{
    public class Explosion : MonoBehaviour
    {
        [SerializeField] private float _radius;
        [SerializeField] private float _force;
        [SerializeField] private Rigidbody[] _chips;
        [SerializeField] private ParticleSystem _effect;

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, _radius);
        }

        public void Explode()
        {
            _effect.transform.SetParent(null);
            _effect.Play();

            foreach (var chip in _chips)
            {
                chip.AddExplosionForce(_force, transform.position, _radius);
            }

            var targets = Physics.OverlapSphere(transform.position, _radius);
            var damageData = new DamageData(15, _force, transform.position);

            foreach (var target in targets)
            {
                if (target.TryGetComponent(out Head damagable))
                {
                    damagable.TakeDamage(damageData);
                }

                target.attachedRigidbody?.AddExplosionForce(_force, transform.position, _radius);
            }
        }
    }
}
