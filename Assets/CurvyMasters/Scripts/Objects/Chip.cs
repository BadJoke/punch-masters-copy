﻿using Enemy;
using Player.Punching;
using UnityEngine;

namespace Objects
{
    public class Chip : MonoBehaviour, IDamagable
    {
        public void TakeDamage(DamageData damageData)
        {
            var direction = transform.position - damageData.Position;
            var force = damageData.Force * direction;
            GetComponent<Rigidbody>().AddForce(force);
        }
    }
}
