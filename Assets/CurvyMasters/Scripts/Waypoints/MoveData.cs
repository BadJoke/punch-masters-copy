﻿namespace Waypoints
{
    public struct MoveData
    {
        public int SubpointIndex;
        public float DistPercentage;
        public float DefaultSpeed;
        public float CurrentSpeed;
        public float Acceleration;

        public MoveData(int subpointIndex, float distPercentage, float defaultSpeed, float currentSpeed, float acceleration)
        {
            SubpointIndex = subpointIndex;
            DistPercentage = distPercentage;
            DefaultSpeed = defaultSpeed;
            CurrentSpeed = currentSpeed;
            Acceleration = acceleration;
        }
    }
}