﻿namespace Waypoints
{
    public class DirectionWaypoint : MovementWaypoint
    {
        protected override PathCreator SetCreator()
        {
            return new DirectionCreator(this);
        }
    }
}