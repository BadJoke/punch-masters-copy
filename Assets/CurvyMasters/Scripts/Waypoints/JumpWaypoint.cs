﻿using UnityEngine;

namespace Waypoints
{
    public class JumpWaypoint : Waypoint
    {
        [Space(10)]
        [SerializeField] private float _jumpAngle;
        [SerializeField] [Range(0.01f, 0.5f)] private float _timeStep = 0.1f;

        public override bool IsStayable => true;
        public override bool IsFightable => false;

        public override float GetSpeed(MoveData data)
        {
            var segmentLength = (Path[data.SubpointIndex + 1] - Path[data.SubpointIndex]).magnitude;
            return segmentLength / _timeStep;
        }

        protected override PathCreator SetCreator()
        {
            return new JumpCreator(this, _jumpAngle, _timeStep);
        }
    }
}