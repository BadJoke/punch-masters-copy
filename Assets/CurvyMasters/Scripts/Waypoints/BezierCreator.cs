﻿using System.Collections.Generic;
using UnityEngine;

namespace Waypoints
{
    public class BezierCreator : PathCreator
    {
        private int _bezierPointsCount;
        private int _bezierSegmentsCount;
        private List<Transform> _bezierPointsPool;
        private List<Transform> _bezierPoints = new List<Transform>();

        public BezierCreator(Waypoint point, int bezierPointsCount, int bezierSegmentsCount, List<Transform> bezierPointsPool) : base(point)
        {
            _bezierPointsCount = bezierPointsCount;
            _bezierSegmentsCount = bezierSegmentsCount;
            _bezierPointsPool = bezierPointsPool;
        }

        public override List<Vector3> Create()
        {
            var _path = new List<Vector3>();

            if (_bezierPoints.Count - 2 != _bezierPointsCount)
            {
                ClearBezierInfo();

                _bezierPoints.Add(Point.transform);
                float pointStep = 1f / (_bezierPointsCount + 1);
                for (int i = 1; i < _bezierPointsCount + 1; i++)
                {
                    float step = i * pointStep;
                    Vector3 point = Vector3.Lerp(Point.Position, Point.NextPoint.Position, step);
                    
                    foreach (Transform bezierPoint in _bezierPointsPool)
                    {
                        if (bezierPoint.gameObject.activeSelf == false)
                        {
                            bezierPoint.gameObject.SetActive(true);
                            bezierPoint.position = point;
                            _bezierPoints.Add(bezierPoint);
                            break;
                        }
                    }
                }

                _bezierPoints.Add(Point.NextPoint.transform);
            }

            float segmentStep = 1f / _bezierSegmentsCount;

            for (int i = 0; i < _bezierSegmentsCount; i++)
            {
                float step = i * segmentStep;
                _path.Add(GetPoint(_bezierPoints, step));
            }

            _path.Add(Point.NextPoint.Position);

            return _path;
        }

        private Vector3 GetPoint(List<Transform> points, float step)
        {
            List<Vector3> positions = new List<Vector3>();

            foreach (Transform point in points)
            {
                positions.Add(point.position);
            }

            while (positions.Count > 1)
            {
                List<Vector3> tempPositions = new List<Vector3>(positions);
                positions.Clear();

                for (int i = 0; i < tempPositions.Count - 1; i++)
                {
                    Vector3 pos = Vector3.Lerp(tempPositions[i], tempPositions[i + 1], step);
                    positions.Add(pos);
                }
            }

            return positions[0];
        }

        private void ClearBezierInfo()
        {
            foreach (Transform point in _bezierPointsPool)
            {
                point.GetComponent<BezierPoint>()?.gameObject.SetActive(false);
            }

            _bezierPoints.Clear();
        }
    }
}