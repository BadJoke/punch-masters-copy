﻿using System.Collections.Generic;
using UnityEngine;

namespace Waypoints
{
    public abstract class PathCreator
    {
        protected Waypoint Point;

        public PathCreator(Waypoint point)
        {
            Point = point;
        }

        public abstract List<Vector3> Create();
    }
}