﻿using UnityEngine;

namespace Waypoints
{
    public abstract class MovementWaypoint : Waypoint
    {
        private Post _post;

        public override bool IsStayable => _post;
        public override bool IsFightable => true;

        private void Awake()
        {
            if (TryGetComponent(out Post post))
            {
                _post = post;
                _post.Cleared += OnPostCleared;
            }
        }

        public override float GetSpeed(MoveData data)
        {
            if (IsStayable && data.DistPercentage < StartCombatThreshold)
            {
                float difference = StartCombatThreshold - StopMovementThreshold;
                float step = (StartCombatThreshold - data.DistPercentage) / difference;
                return Mathf.Lerp(data.CurrentSpeed, 0f, step);
            }

            var t = data.Acceleration * Time.deltaTime;
            return Mathf.Lerp(data.CurrentSpeed, data.DefaultSpeed, t);
        }

        protected abstract override PathCreator SetCreator();

        private void OnPostCleared(Post post)
        {
            post.Cleared -= OnPostCleared;

            _post = null;
        }
    }
}