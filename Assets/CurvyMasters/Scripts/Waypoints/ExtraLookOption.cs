﻿namespace Waypoints
{
    public enum ExtraLookOption
    {
        None = 0,
        NextOfNextPoint = 1,
        SpecialPoint = 2,
    }
}