﻿using UnityEngine;

namespace Waypoints
{
    [ExecuteInEditMode]
    public class BezierPoint : MonoBehaviour
    {
        private Waypoint _waypoint;
        private Vector3 _previousPosition = new Vector3();

#if UNITY_EDITOR
        private void Awake()
        {
            _waypoint = transform.GetComponentInParent<Waypoint>();
        }

        private void Update()
        {
            if (!Application.isPlaying)
            {
                if (_previousPosition != transform.position)
                {
                    _waypoint.CalculateTrajectory();
                    _previousPosition = transform.position;
                }
            }
        }
#endif
    }
}