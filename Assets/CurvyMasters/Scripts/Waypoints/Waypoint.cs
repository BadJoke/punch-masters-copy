using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Waypoints
{
    public abstract class Waypoint : MonoBehaviour
    {
        [SerializeField] private Waypoint _nextPoint;

        [Space(15)]
        [SerializeField] private BaseLookOption _baseLookOption = BaseLookOption.Forward;
        [SerializeField] private ExtraLookOption _extraLookOption = ExtraLookOption.None;
        [SerializeField] private Transform _specialLookPoint;
        [SerializeField] private Transform _combatLookPoint;

        [Space(5)]
        [SerializeField] [Range(1f, 0f)] private float _startRotationThreshold = 0.5f;
        [SerializeField] [Range(1f, 0f)] private float _stopRotationThreshold = 0f;

        [Space(15)]
        [SerializeField] [Range(1f, 0f)] private float _startCombatThreshold = 0.5f;
        [SerializeField] [Range(1f, 0f)] private float _stopMovementThreshold = 0.3f;

        private Vector3 _previousPosition;
        private Vector3 _previousNextPosition;
        private List<Vector3> _path = new List<Vector3>();
        private PathCreator _pathCreator;

        public event UnityAction Reached;

        public abstract bool IsStayable { get; }
        public abstract bool IsFightable { get; }
        public Vector3 Position => transform.position;
        public Waypoint NextPoint => _nextPoint;
        public List<Vector3> Path
        {
            get
            {
                if (_path.Count == 0)
                {
                    CalculateTrajectory();
                }

                return _path;
            }
        }

        public float StartRotationThreshold => _startRotationThreshold;
        public float StopRotationThreshold => _stopRotationThreshold;
        public float StartCombatThreshold => _startCombatThreshold;
        public float StopMovementThreshold => _stopMovementThreshold;

        public BaseLookOption DefaultLookOption => _baseLookOption;
        public ExtraLookOption AddLookOption => _extraLookOption;
        public Transform SpecialLookPoint => _specialLookPoint;
        public Transform CombatLookPoint => _combatLookPoint;

        private void OnEnable()
        {
            CalculateTrajectory();
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            _stopMovementThreshold = Mathf.Clamp(_stopMovementThreshold, 0f, _startCombatThreshold);

            CalculateTrajectory();
        }

        protected void OnDrawGizmos()
        {
            if (PositionWasChanged() || NextPositionWasChanged())
            {
                CalculateTrajectory();
            }

            if (_path.Count > 0)
            {
                Gizmos.color = Color.red;

                for (int i = 0; i < _path.Count - 1; i++)
                {
                    Gizmos.DrawLine(_path[i], _path[i + 1]);
                }
            }

            bool NextPositionWasChanged()
            {
                if (NextPoint != null && NextPoint.Position != _previousNextPosition)
                {
                    _previousNextPosition = NextPoint.Position;
                    return true;
                }
                else
                {
                    return false;
                }
            }

            bool PositionWasChanged()
            {
                if (Position != _previousPosition)
                {
                    _previousPosition = Position;
                    return true;
                }

                return false;
            }
        }
#endif

        public void WasReached()
        {
            Reached?.Invoke();
        }

        public void CalculateTrajectory()
        {
            if (NextPoint == null)
            {
                return;
            }

            _pathCreator = SetCreator();

            _path = _pathCreator.Create();
        }

        public abstract float GetSpeed(MoveData data);

        protected abstract PathCreator SetCreator();
    }
}