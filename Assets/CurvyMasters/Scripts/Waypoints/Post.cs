﻿using Enemy;
using Player;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Waypoints
{
    [RequireComponent(typeof(MovementWaypoint))]
    public class Post : MonoBehaviour
    {
        [SerializeField] private List<Enemy.Enemy> _enemies;
        [SerializeField] private Transform[] _enemyPoints;

        private Waypoint _point;
        private EnemyFactory _factory;
        private PlayerController _player;

        public event UnityAction<Post> Cleared;

        [Inject]
        private void Constructor(EnemyFactory factory, PlayerController player)
        {
            _factory = factory;
            _player = player;
        }

        private void Awake()
        {
            _point = GetComponent<Waypoint>();
            _point.Reached += OnPointReached;

            if (_enemyPoints.Length == 0)
            {
                throw new ArgumentNullException("Empty enemy list on " + gameObject.name);
            }

            foreach (var point in _enemyPoints)
            {
                var enemy = _factory.Create(point.position);
                enemy.transform.LookAt(transform);
                enemy.Died += OnEnemyDied;

                _enemies.Add(enemy);
            }
        }

        private void OnPointReached()
        {
            _point.Reached -= OnPointReached;

            foreach (var agent in _enemies)
            {
                agent.GoToPlayer(_player);
            }
        }

        private void OnEnemyDied(Enemy.Enemy enemy)
        {
            enemy.Died -= OnEnemyDied;

            _enemies.Remove(enemy);

            if (_enemies.Count == 0)
            {
                Cleared?.Invoke(this);
            }
        }
    }
}
