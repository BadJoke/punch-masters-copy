﻿namespace Waypoints
{
    public enum BaseLookOption
    {
        None = 0,
        Forward = 1,
        NextPoint = 2,
    }
}