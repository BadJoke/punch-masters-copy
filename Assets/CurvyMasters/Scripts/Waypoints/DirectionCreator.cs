﻿using System.Collections.Generic;
using UnityEngine;

namespace Waypoints
{
    public class DirectionCreator : PathCreator
    {
        public DirectionCreator(Waypoint point) : base(point) { }

        public override List<Vector3> Create()
        {
            var path = new List<Vector3>();

            path.Add(Point.Position);
            path.Add(Point.NextPoint.Position);

            return path;
        }
    }
}