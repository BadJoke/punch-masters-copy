﻿using System.Collections.Generic;
using UnityEngine;

namespace Waypoints
{
    public class JumpCreator : PathCreator
    {
        private float _jumpAngle;
        private float _trajectoryTimeStep;

        public JumpCreator(Waypoint point, float jumpAngle, float trajectoryTimeStep) : base(point)
        {
            _jumpAngle = jumpAngle;
            _trajectoryTimeStep = trajectoryTimeStep;
        }

        public override List<Vector3> Create()
        {
            var path = new List<Vector3>();
            var direction = Point.NextPoint.Position - Point.Position;
            var angles = Quaternion.LookRotation(direction).eulerAngles;
            var gravity = Physics.gravity * 2f;
            var planeLength = new Vector3(direction.x, 0f, direction.z).magnitude;
            var tiltAngle = angles.x;

            _jumpAngle = AngleClamp(_jumpAngle, tiltAngle);

            float angleInRadians = _jumpAngle * Mathf.PI / 180f;
            float accelerationPow = gravity.y * planeLength * planeLength
                                / (2 * (direction.y - Mathf.Tan(angleInRadians) * planeLength)
                                * Mathf.Pow(Mathf.Cos(angleInRadians), 2));
            float acceleration = Mathf.Sqrt(Mathf.Abs(accelerationPow));

            Vector3 velocity = Quaternion.Euler(-_jumpAngle, angles.y, 0f)
                * Point.transform.forward * acceleration;

            for (int i = 0; i < 50; i++)
            {
                float time = i * _trajectoryTimeStep;
                Vector3 point = Point.Position + velocity * time + gravity * time * time / 2f;

                if (path.Count > 0)
                {
                    Vector3 previousPoint = path[path.Count - 1];
                    float prevToNext = (previousPoint - point).magnitude;
                    float prevToFinal = (previousPoint - Point.NextPoint.Position).magnitude;
                    float nextToFinal = (point - Point.NextPoint.Position).magnitude;

                    if (EquateWithRange(prevToNext, prevToFinal + nextToFinal, 0.5f))
                        break;
                }

                path.Add(point);
            }

            path.Add(Point.NextPoint.Position);

            return path;
        }

        private bool EquateWithRange(float comparedValue, float rangeValue, float range)
        {
            return comparedValue < rangeValue + range && comparedValue > rangeValue - range;
        }

        private float AngleClamp(float value, float calculated)
        {
            float hightLimit = 85f;
            float lowLimit = calculated;

            if (calculated < 180f && calculated > 0f)
            {
                lowLimit = -calculated;
            }
            else if (calculated > 180f && calculated < 360f)
            {
                lowLimit = 360f - calculated + 5f;
            }

            return Mathf.Clamp(value, lowLimit, hightLimit);
        }
    }
}