﻿using System.Collections.Generic;
using UnityEngine;

namespace Waypoints
{
    public class BezierWaypoint : MovementWaypoint
    {
        [SerializeField] [Range(1, 5)] private int _pointsCount = 2;
        [SerializeField] private int _segmentsCount = 30;
        [SerializeField] private List<Transform> _pointsPool = new List<Transform>();

        protected override PathCreator SetCreator()
        {
            return new BezierCreator(this, _pointsCount, _segmentsCount, _pointsPool);
        }
    }
}